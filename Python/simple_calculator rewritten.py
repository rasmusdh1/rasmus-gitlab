import operator
import logging
logging.basicConfig(filename='app.log', filemode='a', format='%(levelname)s - %(message)s', level=logging.WARNING)
ops = {
    "+": operator.add,
    "-": operator.sub,
    "*": operator.mul,
    "/": operator.truediv
} 
#problems: code fails if given anything other than specified operators (probably a way to catch this error)
logging.warning('This is a test log.')
#def add(num1, num2):
#    return num1 + num2

#def sub(num1, num2):
#    return num1 - num2

#def div(num1, num2):
#    return num1 / num2

#def mult(num1, num2):
#    return num1 * num2

def adder(num1, operation, num2):
    return ops[operation](number1,number2)

print('\nWelcome to the simple calculator (write q to quit)')

result = 0.0
chosen_operation = ''

while True:
    print('enter the first number')
    number1 = input('> ')
    logging.warning('%s was inputted.', number1)
    if number1 == 'q':
        print('goodbye...')
        break
    print('enter the second number')
    number2 = input('> ')
    logging.warning('%s was inputted.', number2)
    if number2 == 'q':
        print('goodbye...')
        break
    print('would like to: +, for addition, - for subtraction, * for multiplication and / for division.')
    operation = input('')
    logging.warning('%s was inputted.', operation)
    if operation == 'q':
        print('goodbye...')
        break

    try:
        number1 = float(number1)
        number2 = float(number2)
        result = adder(number1, operation, number2)
        logging.warning('%s was ouputted.', result)
#            result = adder(number1, operation, number2)
#        elif operation == 2:
#            result = adder(number1, operation, number2)
#        elif operation == 3:
#            result = adder(number1 / number2)
#        elif operation == 4:
#            result = adder(number1 * number2)

        print(str(result))
        print('restarting....')

    except ValueError:
        print('Please use proper operators.')
        logging.warning('Value Error')

    except ZeroDivisionError:
        print('cannot divide by zero, please try again')
        logging.warning('Divide by zero error')