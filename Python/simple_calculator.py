#function for addition
def add(num1, num2):
    return num1 + num2

#function for subtraction
def sub(num1, num2):
    return num1 - num2

#function for division
def div(num1, num2):
    return num1 / num2

#function for multiplication.
def mult(num1, num2):
    return num1 * num2

#start message.
print('\nWelcome to the simple calculator (write q to quit)')

#inintal values
result = 0.0
chosen_operation = ''

while True:
    #input for the first number
    print('enter the first number')
    number1 = input('> ')
    #response if input is q (quit)
    if number1 == 'q':
        print('goodbye...')
        break
    #input for the second number
    print('enter the second number')
    number2 = input('> ')
    #response if quitting on the second number.
    if number2 == 'q':
        print('goodbye...')
        break
    #asks the user for which funtion they would like to use with the 2 previous inputted numbers.
    print('would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?')
    operation = input('> ')
    #quit response
    if operation == 'q':
        print('goodbye...')
        break

    try:
        #defines the input as floats
        number1 = float(number1)
        number2 = float(number2)
        #defines the operator as an integer.
        operation = int(operation)
        #what to do if the operation integer is 1 (addition)
        if operation == 1:
            result = add(number1, number2)
            chosen_operation = ' added with '
        #what to do if the operation integer is 2 (subtraction)
        elif operation == 2:
            result = sub(number1, number2)
            chosen_operation = ' subtracted from '
        #what to do if the operation integer is 3 (division)
        elif operation == 3:
            result = div(number1, number2)
            chosen_operation = ' divided with '
        #what to do if the operation integer is 4 (multiplication)
        elif operation == 4:
            result = mult(number1, number2)
            chosen_operation = ' multiplied with '
        #prints the result of the operation and prints a message before restarting
        print(str(number1) + chosen_operation + str(number2) + ' = ' + str(result))
        print('restarting....')
    #error if number1 or number2 is not q or a number (can't convert it to a float if it isn't a number, and q is the only letter for which it has a response.)
    except ValueError:
        print('only numbers and "q" is accepted as input, please try again')
    #divide by zero error.
    except ZeroDivisionError:
        print('cannot divide by zero, please try again')
