# Opsætningen af markdown pages

## Opgave: sæt markdown pages op

Her er mit dokumentation for opsætningen af denne side.

Beskrivelse: opgaven gik ud på at sætte en markdown page op.

Hvordan jeg gjorde det: efter at have opsat min GitLab pages, connectede jeg mit repository til Visual Studio Code, hvorefter jeg lavede "new.md" (navnet på denne side), for at teste om det virkede. Efter det gjorde det, begyndte jeg at fylde det her information ud.

Problemer jeg løb ind i: GitLab Pages ikke opdaterede med nyt tekst jeg havde skrevet. Har ingen ide hvad der blev ændret for at det nu er opdateret.

Resourcer: Jeg fulgte guiden i opgave beskrivelsen.

Erfaring: Jeg er nu bekendt med opsætningen og brug af markdown pages.

