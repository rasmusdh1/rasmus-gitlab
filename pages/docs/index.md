# Rasmus selvvalgt projekt

Dette er hvor alt information vedrørende mit selvvalgt projekt kan findes.

For mit projekt, har jeg valgt at fokuser på pentesting ved brug af [VulnHub](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/) til at køre [DVWA](https://github.com/digininja/DVWA).

## Information omkring projektet

### Lærings mål

[Lærings mål kan findes her.](projekt/læringsmål.md)

### Projekt mål

[Et kort overblik over projektmål kan findes her.](projekt/projektmål.md)

### Log

[En log over projekt forløbet kan findes her.](projekt/log.md)

## Produktet

### Low sikkerhedsniveau

[Opgaver lavet på "Low" sikkerhedsniveau kan findes her.](projekt/nået.md)

### Medium sikkerhedsniveau

[Opgaver lavet på "Medium" sikkerhedsniveau kan findes her.](projekt/nåetmedium.md)

### High sikkerhedsniveau

 [Opgaver lavet på "High" sikkerhedsniveau kan findes her.](projekt/nåethigh.md)