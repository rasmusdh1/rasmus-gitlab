# Viden:

    - Gængse sårbarheder i webapplikationer.
    - Forståelse omkring hvilken dele af en webapplikation er sårbar.
        - f.eks. SQL databaser

# Færdigheder:

    - Brug af et opsat environment (VulnHub), til at gennemføre tests mod en række forskellige applikationer for at teste og komme igennem de sikkerhedsforanstaltninger der er i plads.

# Kompetencer:

    - Bruge viden af sårbarheder til at kunne finde steder i VulnHub's applikationer hvor der er mangel sikkerheds-mæssigt, or derfra udnytte dem til at udføre opgave, hvis nogen, der er sat ind i de applikationer.
    - Selv opnå viden indenfor både nye og allerede udforsket emner.