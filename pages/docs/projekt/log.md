# 03/03/24
- Webgoat
    - HTTP basics
    - Access Control Flaws
    - AJAX Security
    - Authentication Flaws

# 04/03/24
- Vejledning
    - Hvad der skal være i "produktet"
    - Projektmål
    - Læringsmål
        - Specificering?
- Burde gøres til næste vejledning:
    - alt med DVWA low burde være færdig(!!)
- Burde gøres til/på næste mandag:
    - Startet på DVWA.
    - Lavet en (loose) timeline.

# 11/03/24
- Projekt skrivning begyndt
    - bare hurtige noter til de forskellige punkter
- DVWA
    - Progress kan ses under [Denne side](nået.md)
        - Brute Force
        - Command Injection
        - CSRF
        - File Upload

# 17/03/24
- Færdig med low level DVWA
    - SQL injection
    - SQL injection (blind)
    - Weak Session IDs
    - XSS (DOM)
    - XSS (Reflected)
    - XSS (Stored) 
    - CSP Bypass
    - JavaScript
- Regner med at være færdig med, i det mindste, DVWA medium i påsken.
    - Regner også med at have skrevet en god del når vi kommer ud af den.

# 28/03/24
- Gået i gang med medium level
    - Brute Force
    - Command Injection
    - CSRF
    - File Upload

# 29/03/24
- Medium level
    - SQL injection
    - SQL injection (blind)
    - Weak Session IDs

# 07/04/24
- Arbejdet på produktet
- Skrevet i rapporten

# 12/04/24
- Skrevet i rapporten

# 14/04/24
- Arbejdet på high level
- Gjort produktet lidt pænere
- Spørgsmål:
    - Indledning vs. valg af emne
    - Hvor overordnet er overordnet
    - Produkt
    - Se på læringsmål/projektmål

# 15/04/24
- Markdown ændringer for DVWA Low

# 18/04/24
- Skrevet i rapporten.

# 20/04/24
- Skrevet i rapporten.

# 21/04/24
- Skrevet i rapporten.

# 22/04/24
- Skrevet i rapporten.
- Produkt ændringer.

# 23/04/24
- Skrevet i rapporten.
- Produkt ændringer.

# 24/04/24
- Skrevet i rapporten.

# 25/04/24
- Aflevering.