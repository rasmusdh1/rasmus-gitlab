Nuværende projekt mål er at komme igennem DVWA Low og Medium, og så se hvor langt jeg kan nå at komme igennem High.
- Alle DVWA opgaver undtaget:
    - File Inclusion
        - Irriterende og kompleks at gå ind i docker containeren for DVWA og ændre på filer. 
    - Captcha
        - Forældet.
        - Skal have en API key.

# 17/03:
- Blev færdig med Low Level

# Påsken
- Færdig med medium level, startet på high

# 14/04/24
- Startet på High Level