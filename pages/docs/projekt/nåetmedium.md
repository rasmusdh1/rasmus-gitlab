# Brute Force
Mål: brute force admin kontoens password.

Ny sikkerhed: 2 sekunders ekstra ventetid på forkete login oplysninger

Ide: samme som i low level, brug af Burp Suite's repeater
Resultat: ![BruteForce](../images/DVWAmedium/bruteforcedone.png)
Reflektion: viser at metoden der blev brugt på low-level nok var en smule overkill, samt hvorfor det er en god ide at have ekstra sikkerhedsforanstaltninger såsom captcha eller MFA.

# Command Injection
Mål: at execute en command udover ping.

Ny sikkerhed: som kan ses i source code for siden, er && og ; blacklisted. 

![blacklist](../images/DVWAmedium/commandinjectionblacklist.png)

Videns behov: andre måde at chain commands på.

- https://dev.to/bitecode/run-multiple-commands-in-one-line-with-and-linux-tips-5hgm
    - ||
        - Kører kun når den første command fejler.

Ide: få ping command til at fejle med "a || ls."

Resultat: 

![commandinjection](../images/DVWAmedium/commandinjectiondone.png)

Refleksioner: klart at se hvorfår det er nødvendigt at have god input sanitering. Udover || er det også muligt at bruge en enkelt & til at "background" den anden command.

![alternativ løsning](../images/DVWAmedium/commandinjectionalternativ.png)

# CSRF
Mål: at ændre password ved at en bruger klikker på et link.

Ny sikkerhed: alle requests der ikke har den rigtige referer header bliver denied.
- "[...] there is a check to see where the last requested page came from."

Videns behov: Viden om referer header.

- https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referer
    - Dette hjalp ikke så meget.
- Grund ideen er at DVWAs IP skal være under referer.
    - DVWAs hjælp siger at der kan bruges reflektive XSS til denne opgave.
        - Finde en måde at execute et script der ændre password inde på selve DVWAs side.
- https://owasp.org/www-community/attacks/xss/
    - Kiggede på en guide da jeg ikke finde på nogen løsningen for hvordan jeg ville execute XSS igennem DVWA, løsningen var rimelig åbenlys (brug XSS rummet til det.)
        - https://github.com/mzet-/ctf-writeups/blob/master/DVWA/dvwa-medium.md
- '< img src="http://192.168.209.129:1335/vulnerabilities/csrf/?password_new=password&password_conf=password&Change=Change#">'

Resultat: ![CSRF](../images/DVWAmedium/csrfdone.png)

# File Upload
Mål: eksekver en PHP commando gennem fil uploading.

Ny sikkerhed: tager kun imod JPEG eller PNG filer.

Første ide: lav en fil med phpinfo(INFO_CREDITS) og ændre fil extension til .png

- lykkede desværre ikke.

Anden ide: kig request igennem i Burp Suite.

- Har "filename="medium.jpg"
- https://www.acunetix.com/websitesecurity/upload-forms-threat/
    - Nævner ".php.jpg"
    - Hvis jeg uploader med "medium.php.jpg" og ændre filnavnet i requesten til bare .php

Resultat: ![fileupload](../images/DVWAmedium/fileuploaddone.png)

# SQL Injection

![SQLinjectionMediumOpgave](../images/DVWAmedium/SQLinjectionMediumOpgave.png)

Mål: at få alle brugernes username og passwords.

Ny sikkerhed: drop down menu, med "mysql_real_escape_string"

- "The medium level uses a form of SQL injection protection, with the function of "mysql_real_escape_string()"."

Ide: find ud af om det er muligt at escape.

- Ved brugen af ```1 OR 1=1; -- ``` får vi en liste af alle brugere, hvilket betyder at det er muligt for os at lave injection angreb.

![alt text](../images/DVWAmedium/SQLfirstinjection.png)

Hvis vi gør ligesom i low level opgaven, kan vi bruge "1 UNION SELECT table_name, NULL FROM information_schema.tables --" til at finde listen over alle de tabeller der er i databasen.

![alt text](../images/DVWAmedium/SQLtabelinjection.png)

Uheldigvis, på grund af den forhøjede sikkerhed, kan vi ikke se hvad der er under users, så vi må se efter en anden vinkel.
Hvis vi ser på de links DVWA har sat under opgaven, får vi dette cheatsheet: https://pentestmonkey.net/cheat-sheet/sql-injection/mysql-sql-injection-cheat-sheet
Hvis vi prøver at indsende ```1 UNION SELECT user FROM users --```, får vi svaret "The used SELECT statements have a different number of columns." Hvilket antyder vi har brug for flere SELECT arguments.
Hvis vi prøver den anden af dem ```1 UNION SELECT host, user, password FROM users --``` får vi "Unknown column 'host' in 'field list'"
Hvis vi derefter fjerner 'host' fra query, så vi står tilbage med ```1 UNION SELECT user, password FROM users```:

![SQL injection](../images/DVWAmedium/SQLinjectiondone.png)

# SQL injection (blind)

Mål: at finde versionen af SQL databasen.

Ny sikkerhed: samme som i SQL injection.

Ide: da sikkerheden er den samme som i den tidligere opgave, ved vi at injection såsom ```1 OR 1=1; --```, dette giver svaret tilbage "User ID exists in the database.", hvilket bliver vores TRUE statement, hvorimod "User ID is MISSING from the database." bliver vores FALSE statement.
Ide: opstil query der tager det første character af ```@@version```, og sammenligner det med alle ASCII værdier for characters:

- ```1 1 AND ASCII(SUBSTR(VERSION(),3,3))>[VALUE] --```
    - Ved brug af Burp Suite intruder er det muligt at køre alle characters igennem for at se om nogen af dem giver TRUE respons.
        - Dette er dog meget tidskrævende.
    - ![første character](../images/DVWAmedium/SQLblindfirstcharacter.png)
    - Første character må derfor have en ASCII value af 49, som er 1.
    ![alt text](../images/DVWAmedium/SQLblindsecondcharacter.png)
    ![alt text](../images/DVWAmedium/SQLblindthirdcharacter.png)
    Har ikke tænkt mig at gå alle characters igennem da det ville tag lang tid.

# Weak Session IDs
Mål: at finde mønstret af "dvwaSession"

Ny sikkerhed: længere integer.

Ide: generate en masse og se om jeg kan finde ud af mønstret.

- 1711748747
- 1711748757
- 1711748767
- 1711748774
- 1711748780
- 1711748788
- 1711748804
- 1711748832

Første tanke var at de bare gik op med 10 hver gang, men som jeg generatede flere blev den teori ødelagt.
Faktisk er det baseret på tiden, specifikt den Date der er på response.

![alt text](../images/DVWAmedium/weaksessionIDsmedium.png)

Betyder det ville være muligt at reverse-engineer og lav session ID hvis du vidste hvornår en person fik deres.

# XSS (DOM)
Mål: at få en brugers cookies

Ny sikkerhed: filter der denier alle URLs med ```<script>```
Da ```<script>``` ikke virker længere vidste jeg at jeg blev nød til at finde en anden måde at få hjemmesiden til at display cookies på.

Første tanke var at gå idn på OWASP's cheatsheet og prøve nogle af de andre af.

https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/XSS_Filter_Evasion_Cheat_Sheet.md

Efter at have prøvet alle dem uden ```<script>``` af, og ingen af dem virkede, tog jeg brug af det første "hint" under DVWA's help.

- "You must first break out of the select block [...]"
    - Fandt ud af at dette talte om HTML blocks, så gik ud fra at jeg skulle bruge ```</select>```

        ![ChatGPTXSS](../images/DVWAmedium/ChatGPTXSS.png)

        - Med dette brugte jeg OWASP's cheatsheet igen, og gik igennem indtil jeg fandt en der virkede.
            - ```</select><IMG SRC=javascript:alert(String.fromCharCode(88,83,83))>```
            ![alt text](../images/DVWAmedium/XSSdomsuccess.png)
            - Ved at kigge på det script jeg brugte i low level security, får jeg "document.write ("This is remote text via xss.js located at xss.rocks " + document.cookie);", og ved at kombiner det med det tidligere: ```</select><IMG SRC=javascript:alert(document.cookie)>```
                - Det virkede ikke, men ved at bruge "On Error" i stedet for:
                    - ```</select><"IMG SRC=/ onerror="alert(document.cookie)"></img>```

                    ![success](../images/DVWAmedium/XSSdomcookies.png)

# XSS (Reflected)

Mål: at få en brugers cookies

Ny sikkerhed: "The developer has tried to add a simple pattern matching to remove any references to ```"<script>"```, to disable any JavaScript."

Her bruger vi samme script som i tidligere opgave:
![Reflected](../images/DVWAmedium/XSSreflectedsuccess.png)

# XSS (Stored)

Mål: at opsætte en redirect

Ny sikkerhed: Input sanitering
- "The developer had added some protection, however hasn't done every field the same way."

Som beskrivelsen for medium level siger, så er der tilføjet nogle nye sikkerhedsforanstaltninger, men ikke alle felterne er gjort på samme måde.

![alt text](../images/DVWAmedium/XSSstoredsource.png)

Som kan ses, er der en forskel på de to input felter.
"message" felter, har en meget strengerer type, hvorimod at "name" feltet bare har en simpel string replacment hvis den ser ```<script>```
Hvis vi bruger det der blev gjort i de to tidligere opgave som skabelon, kan vi indrage metoden fra low level til at skabe:

- ```<IMG SRC=/ onerror=window.location.replace("https://www.google.com")></img>```
    - Vi bliver dog nød til at bruge Burp Suite da der er client-side limit på hvor lang en string man kan indsætte som navn.

![alt text](../images/DVWAmedium/XSSstoresburpsuite.png)

Effekten af dette:

![alt text](../images/DVWAmedium/XSSstoresredirect.png)
- Note "Referer"

# CSP Bypass

Mål: at få et script til at køre på hjemmesiden.

Ny sikkerhed: Ny CSP nonce der prøver at stoppe scripts.

- "The CSP policy tries to use a nonce to prevent inline scripts from being added by attackers."

I Source kan vi se CSP policien: ```Content-Security-Policy: script-src 'self' 'unsafe-inline' 'nonce-TmV2ZXIgZ29pbmcgdG8gZ2l2ZSB5b3UgdXA=';```
Fra dette kan vi se "script-src 'self'", hvilket betyder at hvis vi kan få en fil på serveren kan vi nemt få den til at køre.
Heldigvis er der en fil upload funktion i DVWA, og ved at uploade det script fra OWASP's Cheat Sheet, kan vi nemt køre det (ved først at komme hen til den rigtige path)

- ```<"SCRIPT SRC=../../hackable/uploads/index.js></"SCRIPT>```

![alt text](../images/DVWAmedium/CSPBypassScript.png)

# JavaScript

Mål: at submit ordet "success"

Ny sikkerhed: ikke rigtig noget med sikkerhed at gøre det her, mere en slags gåde.

Inde i Burp Suite kan man se at hvert POST har en token i sig, "XXeMegnahCXX" (ChangeMe bagvens), ved at ændre det til "XXsseccusXX" (success bagvent) får vi:

![alt text](../images/DVWAmedium/JavaScriptmedium.png)

Refleksion: rimelig spild af tid.