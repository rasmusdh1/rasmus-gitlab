# Opgave om BOLA og EDE 

## Få en anden kontos køretøjs lokation (BOLA)

Først, opret 2 brugere som specifieret i opgaven. Derefter logges ind på Bruger 1 gennem Burp Suite (gennem proxy) mens tracking er on. Kom ind til dashboard, og derefter refresh bilens lokation. Check Burp Suite for noget der ligner "/identity/api/v2/vehicle/316d19c3-489e-491a-a4c2-f553cffbecde/location". Noter det ned, specifikt "316d19c3-489e-491a-a4c2-f553cffbecde" delen. Log in på bruger 2, og kom ind til dashboardet. Refresh der lokation på bilen, og når der kommer en request der ligner den tidligere, replace Bruger 2s med Bruger 1s.

## Få information om andre (EDE)

Først, log in med en bruger, og gå ind på shoppen. Derefter lav en ordre og gå ind på order details. Brug Burp Suite til at ændre ordre ID til andet (1 og 2 har information om andre brugere). Order 1 giver "test@example.com" og en telefon på "9876540001". (Kan også bare ændre ordre IDen i URL.). 

## Få en brugers bil lokation (bruger som ikke selv har lavet)

Under HTTP history når du indlæser en community post er det muligt at finde vechicle ID for andre brugere, og derved kan der bruges methoden vist i første opgave til at få placeringen på bilen af en bruger der "a)" ikke er mig "b)" ikke en bruger jeg har oprettet.

## ASVS Access Control

"Verify that all user and data attributes and policy information used by access 
controls cannot be manipulated by end users unless specifically authorized."

"Verify that the principle of least privilege exists - users should only be able to 
access functions, data files, URLs, controllers, services, and other resources, 
for which they possess specific authorization. This implies protection against 
spoofing and elevation of privilege."

## Mechanic reports

I Burp Suite, men intercept på, brug URL'en til en report (/workshop/api/mechanic/mechanic_report?report_id=4) og edit GET requesten ved at filføje authoriztaion cookie.